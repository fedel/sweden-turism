var visitedPlaces = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Copenhaguen - 04/21/19 <br> We visited Kastellet, little mermaid monument and the King's Garden <br> 06/08/19 - We watched the movie Marighella at Cinemateket <br> 07/27/19 - Eliza and Thais visted Cristiania a hippie and colorfull neighborhood."
            },
            "geometry": {
                "type": "Point", 
                "coordinates": [12.5700724, 55.6867243]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Dalby - 05/01/19 <br> We visited the Dalby Söderskog National Park, the smalles National Park in Europe. We went by bike."
            },
            "geometry": {
                "type": "Point", 
                "coordinates": [13.3461589, 55.6646637]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Uppåkra - 05/11/19 <br> There is a archeological site. We went by bike."
            },
            "geometry": {
                "type": "Point",  
                "coordinates": [13.16934, 55.66629]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Lomma - 05/18/19 <br> A beatiful city, with an amazing beach. We went by bike and visited the pier and the botanical garden"
            },
            "geometry": {
                "type": "Point",   
                "coordinates": [13.0708928, 55.6745348]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Söderåsen National Park - 05/30/19 <br> We went by train to Stehag station and then toke a bus to the park. It is very nice, and there are different trails, all of them marked (so you can go by yourself)"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [13.232, 56.0291]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Malmö - 06/01/19 <br> We went by train, visited the Torning Torso and we were in a Food truck festival                                     <br> 06/06/19 We were in Ribersborg beach"
            },
            "geometry": {
                "type": "Point",  
                "coordinates": [13.0001566, 55.6052931]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Lund - Start point. We visted many places: Kulturen, Stadsparken, Sankt Hans Hill, Botanical Garden, Tunaparken, Lund Cathedral "
            },
            "geometry": {
                "type": "Point", 
                "coordinates": [13.1929, 55.7018]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Falsterbo - 06/22/2019 <br> We went by bus, and we made a long hiking."
            },
            "geometry": {
                "type": "Point", 
                "coordinates": [12.8371665, 55.3960900]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Höllviken - 06/26/2019 <br> Eliza went with her friends. A beach more beautiful than Falsterbo"
            },
            "geometry": {
                "type": "Point", 
                "coordinates": [12.9632, 55.4024]
            }
        },

        {
            "type": "Feature",
            "properties": {
                "popupContent": "Ales Stenar - 06/29/19 <BR> We went by train to Ystad and then by bike to Ales Stanar (18 km). The place is very beautiful and windy. We had lunch in Ystad before the cycling."
            },
            "geometry": {
                "type": "Point",  
                "coordinates": [14.05439, 55.38266]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Helsinborg - 07/14/19 <BR> We went by train and visited the Sofiero Pallace and Garden, its garden is amazing! <br> 07/26/19 - Eliza went with Tais and they visited Kärnan Tower, a great view of the bay!"
            },
            "geometry": {
                "type": "Point",   
                "coordinates": [12.7040684, 56.0441984]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "popupContent": "Ystad - 06/25/19 <BR> Eliza and Thais went there by train, it is such a cute place with old churches and Danish houses"
            },
            "geometry": {
                "type": "Point",  
                "coordinates": [13.8196555, 55.4329849]
            }
        },

        {
            "type": "Feature",
            "properties": {
                "popupContent": "Hallands Väderö - 08/03/2019 <br> We reached there using train, bus and a ferry. It is important to note the ferry-boat time (there are not so much, and the last is not so late). Check more information about the ferry-boat here: https://www.vaderotrafiken.se"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [12.564, 56.4459]
            }
        }
    ]
};
